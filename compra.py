habitual = ("patatas", "leche", "pan")


def main():
    especifica = []

    print("Elemento a comprar: ")

    while True:
        producto = input()
        if not producto:
            break

        if producto not in habitual:
            especifica.append(producto)

    print("\nLista de la compra:")

    for producto in habitual:
        print(producto)

    for producto in especifica:
        print(producto)

    print(f"\nTotal elementos habituales: {len(habitual)}")
    print(f"Total elementos específicos: {len(especifica)}")


if __name__ == "__main__":
    main()
